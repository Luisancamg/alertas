package com.example.alertas.ViewModels;

import java.util.List;

public class Alertas {

    public List<Alertas_detalle> alertas;

    public List<Alertas_detalle> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alertas_detalle> alertas) {
        this.alertas = alertas;
    }
}

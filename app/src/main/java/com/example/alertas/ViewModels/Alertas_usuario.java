package com.example.alertas.ViewModels;

import java.util.List;

public class Alertas_usuario {

    public String estado;
    public String detalle;
    public List<Alertas_detalle> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public List<Alertas_detalle> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<Alertas_detalle> alertas) {
        this.alertas = alertas;
    }
}

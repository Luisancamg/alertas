package com.example.alertas.ViewModels;

import java.util.List;

public class visualizacionesmodel {

    public List<Visualizaciones_detalles> visualizaciones;

    public List<Visualizaciones_detalles> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(List<Visualizaciones_detalles> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}

package com.example.alertas.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.R;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.Peticion_Login;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private TextView Crearcuenta;
    private EditText Correo;
    private EditText Contraseña;
    private Button Login;
    private String APITOKEN;
    private int IDUSUARIO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        Crearcuenta=(TextView) findViewById(R.id.txtcrear);
        Correo=(EditText) findViewById(R.id.edtcorreo);
        Contraseña=(EditText) findViewById(R.id.edtcontraseña);
        Login = (Button) findViewById(R.id.btnsesion);
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=preferencias.getString("Token","");
        if (token != ""){
            startActivity(new Intent(MainActivity.this, Menu.class));
            Toast.makeText(this,"Su sesion aun esta activa", Toast.LENGTH_SHORT).show();
        }

        Crearcuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Registro = new Intent(MainActivity.this,Registro.class);
                startActivity(Registro);
            }
        });
Login.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if (TextUtils.isEmpty(Correo.getText().toString()))
            Correo.setError("Este campo no puede estar vacio");
        if (TextUtils.isEmpty(Contraseña.getText().toString()))
            Contraseña.setError("Este campo no puede estar vacio");
        else{
            servicioPeticion service = Api.getApi(MainActivity.this).create(servicioPeticion.class);
            Call<Peticion_Login> peticion_loginCall= service .login(Correo.getText().toString(),Contraseña.getText().toString());
            peticion_loginCall.enqueue(new Callback<Peticion_Login>() {
                @Override
                public void onResponse(Call<Peticion_Login> call, Response<Peticion_Login> response) {
                    Peticion_Login peticion=response.body();
                    if(response.body()==null){
                        Toast.makeText(MainActivity.this,"Ocurrio un error, intentalo más tarde :(", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if(peticion.estado == "true"){
                        APITOKEN=peticion.token;
                        IDUSUARIO=peticion.id;

                        guardarPreferencias();
                        startActivity(new Intent(MainActivity.this, Menu.class));
                        Toast.makeText(MainActivity.this,"Usted ha iniciado sesion :) ", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(MainActivity.this,peticion.estado, Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<Peticion_Login> call, Throwable t) {
                    Toast.makeText(MainActivity.this,"No podemos conectarnos :(", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
});
    }
    private void guardarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor= preferences.edit();
        editor.putString("Token",APITOKEN);
        editor.putString("Idusuario", String.valueOf(IDUSUARIO));
        editor.commit();
    }
}

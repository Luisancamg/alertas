package com.example.alertas.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.R;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.visualizacionesmodel;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizaciones extends AppCompatActivity {


    final ArrayList<Integer> listaalertas = new ArrayList<>();
    final ArrayList<Integer> listaidUsuarios = new ArrayList<>();
    final ArrayList<Integer> listaidalerta = new ArrayList<>();
    final ArrayList<String> fecha_creacion = new ArrayList<>();
    final ArrayList<String> fecha_actualizacion = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizaciones);
        final ListView lista;
        lista = (ListView) findViewById(R.id.ListVisualizacion);
        servicioPeticion service = Api.getApi(Visualizaciones.this).create(servicioPeticion.class);
        Call<visualizacionesmodel> visualizacionesCall= service .Visualizaciones();
        visualizacionesCall.enqueue(new Callback<visualizacionesmodel>() {
            @Override
            public void onResponse(Call<visualizacionesmodel> call, Response<visualizacionesmodel> response) {
                final visualizacionesmodel peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(Visualizaciones.this, "Ocurrio un error, intentalo más tarde", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.visualizaciones != null) {
                    for (int i = 0; i < peticion.visualizaciones.size(); i++) {
                        listaalertas.add(peticion.visualizaciones.get(i).id);
                        listaidUsuarios.add(peticion.visualizaciones.get(i).usuarioId);
                        listaidalerta.add(peticion.visualizaciones.get(i).alertaId);
                        fecha_creacion.add(peticion.visualizaciones.get(i).created_at);
                        fecha_actualizacion.add(peticion.visualizaciones.get(i).updated_at);
                    }
                    ArrayAdapter adapter = new ArrayAdapter(Visualizaciones.this, android.R.layout.simple_list_item_1, listaalertas);
                    lista.setAdapter(adapter);
                } else {
                    Toast.makeText(Visualizaciones.this, "ocurrio un error", Toast.LENGTH_SHORT).show();
                }
            }

                @Override
                public void onFailure (Call < visualizacionesmodel > call, Throwable t){
                    Toast.makeText(Visualizaciones.this, "No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
                }

        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidalerta.get(position), listaidUsuarios.get(position), fecha_creacion.get(position), fecha_actualizacion.get(position));
            }
        });
    }


    private void obtenerinformacion(Integer idalerta,Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Visualizaciones.this);
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion+"\n" +
                "id alerta: "+idalerta);
        builder.setPositiveButton("Aceptar", null);
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

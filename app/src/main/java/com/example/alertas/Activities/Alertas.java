package com.example.alertas.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.R;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.Alertas;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class alertas extends Fragment {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private String mParam1;
    private String mParam2;

    public alertas() {

    }



    public static alertas newInstance(String param1, String param2) {
        alertas fragment = new alertas();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.activity_alertas, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ListView lista;
        final ArrayList<Integer> listaalertas=new ArrayList<>();
        final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
        final ArrayList<String> fecha_creacion=new ArrayList<>();
        final ArrayList<String> fecha_actualizacion=new ArrayList<>();
        lista=view.findViewById(R.id.Listalertas);

        servicioPeticion service = Api.getApi(this.getActivity()).create(servicioPeticion.class);
        Call<Alertas> AlertasCall= service .Alertas();
        AlertasCall.enqueue(new Callback<Alertas>() {
            @Override
            public void onResponse(Call<Alertas> call, Response<Alertas> response) {
                final Alertas peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getContext(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;

                }
                if(peticion.alertas != null){
                    for(int i=0;i<peticion.alertas.size();i++){
                        listaalertas.add(peticion.alertas.get(i).id);
                        listaidUsuarios.add(peticion.alertas.get(i).usuarioId);
                        fecha_creacion.add(peticion.alertas.get(i).created_at);
                        fecha_actualizacion.add(peticion.alertas.get(i).updated_at);

                    }
                    ArrayAdapter adapter=new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,listaalertas);
                    lista.setAdapter(adapter);

                }else{
                    Toast.makeText(getContext(),"ocurrio un error",Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Alertas> call, Throwable t) {
                Toast.makeText(getContext(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });
    }

    private void obtenerinformacion(Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

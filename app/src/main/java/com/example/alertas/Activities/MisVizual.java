package com.example.alertas.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.R;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.Visualizaciones_usuario;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisVizual extends AppCompatActivity {

    private String Usuariotxt;
    private int idusuario=0;
    final ArrayList<Integer> listaalertas=new ArrayList<>();
    final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
    final ArrayList<Integer> listaidalerta=new ArrayList<>();
    final ArrayList<String> fecha_creacion=new ArrayList<>();
    final ArrayList<String> fecha_actualizacion=new ArrayList<>();
    private ListView listaVisualizaciones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_vizual);
        listaVisualizaciones = (ListView) findViewById(R.id.Listmv);
        obtenerid();
        LlenarListview();
        listaVisualizaciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaalertas.get(position),listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });

    }
    public void obtenerid(){
        SharedPreferences preferencias=MisVizual.this.getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }
    public void LlenarListview() {
        servicioPeticion service = Api.getApi(MisVizual.this).create(servicioPeticion.class);
        Call<Visualizaciones_usuario> AlertasCall = service.Visualizacionesusuario(idusuario);
        AlertasCall.enqueue(new Callback<Visualizaciones_usuario>() {
            @Override
            public void onResponse(Call<Visualizaciones_usuario> call, Response<Visualizaciones_usuario> response) {
                final Visualizaciones_usuario peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(MisVizual.this, "Ocurrio un error, intentalo más tarde", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estado == "true") {
                    listaalertas.clear();
                    listaidUsuarios.clear();
                    listaidalerta.clear();
                    fecha_creacion.clear();
                    fecha_actualizacion.clear();
                    for (int i = 0; i < peticion.visualizaciones.size(); i++) {
                        listaalertas.add(peticion.visualizaciones.get(i).id);
                        listaidUsuarios.add(peticion.visualizaciones.get(i).usuarioId);
                        listaidalerta.add(peticion.visualizaciones.get(i).alertaId);
                        fecha_creacion.add(peticion.visualizaciones.get(i).created_at);
                        fecha_actualizacion.add(peticion.visualizaciones.get(i).updated_at);
                    }
                    ArrayAdapter adapter = new ArrayAdapter(MisVizual.this, android.R.layout.simple_list_item_1, listaalertas);
                    listaVisualizaciones.setAdapter(adapter);
                } else {
                    Toast.makeText(MisVizual.this, peticion.detalle, Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<Visualizaciones_usuario> call, Throwable t) {
                Toast.makeText(MisVizual.this, "No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void obtenerinformacion(Integer idalerta,Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MisVizual.this);
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion +"\n" +
                "id Alerta: "+ idalerta);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

}

package com.example.alertas.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.alertas.R;

public class Menu extends AppCompatActivity {

    Button btna,btnma,btnvi,btnmvi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu2);

      btna=(Button) findViewById(R.id.btna);
      btnma=(Button) findViewById(R.id.btnma);
      btnvi=(Button) findViewById(R.id.btnvi);
      btnmvi=(Button) findViewById(R.id.btnmvi);


      btna.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              startActivity(new Intent(Menu.this, alertas.class));
          }
      });

      btnma.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              startActivity(new Intent(Menu.this, MiAlerta.class));
          }
      });

      btnvi.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              startActivity(new Intent(Menu.this, Visualizaciones.class));
          }
      });

      btnmvi.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              startActivity(new Intent(Menu.this, MisVizual.class));
          }
      });
    }
}

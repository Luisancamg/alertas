package com.example.alertas.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alertas.Api.Api;
import com.example.alertas.R;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.Alertas_usuario;
import com.example.alertas.ViewModels.Peticion_crear_alerta;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MiAlerta extends AppCompatActivity {

    private String Usuariotxt;
    private int idusuario=0;
    final ArrayList<Integer> listaalertas=new ArrayList<>();
    final ArrayList<Integer> listaidUsuarios=new ArrayList<>();
    final ArrayList<String> fecha_creacion=new ArrayList<>();
    final ArrayList<String> fecha_actualizacion=new ArrayList<>();
    private ListView listaAlertas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mi_alerta);
        final Button CrearAlerta;
        CrearAlerta = (Button) findViewById(R.id.btnalerta);
        listaAlertas = (ListView) findViewById(R.id.Listmialerta);
        obtenerid();
        LlenarListview();

        listaAlertas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                obtenerinformacion(listaidUsuarios.get(position),fecha_creacion.get(position),fecha_actualizacion.get(position));
            }
        });
        CrearAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listaAlertas.setAdapter(null);
                CrearAlerta();
                LlenarListview();
            }
        });

    }
    public void obtenerid(){
        SharedPreferences preferencias=MiAlerta.this.getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }

    public void LlenarListview(){
        servicioPeticion service = Api.getApi(MiAlerta.this).create(servicioPeticion.class);
        Call<Alertas_usuario> AlertasCall= service .Alertasusuario(idusuario);
        AlertasCall.enqueue(new Callback<Alertas_usuario>() {

            @Override
            public void onResponse(Call<Alertas_usuario> call, Response<Alertas_usuario> response) {
                final Alertas_usuario peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(MiAlerta.this, "Ocurrio un error, intentalo más tarde", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estado == "true") {
                    listaalertas.clear();
                    listaidUsuarios.clear();
                    fecha_creacion.clear();
                    fecha_actualizacion.clear();
                    for (int i = 0; i < peticion.alertas.size(); i++) {
                        listaalertas.add(peticion.alertas.get(i).id);
                        listaidUsuarios.add(peticion.alertas.get(i).usuarioId);
                        fecha_creacion.add(peticion.alertas.get(i).created_at);
                        fecha_actualizacion.add(peticion.alertas.get(i).updated_at);
                    }
                    ArrayAdapter adapter = new ArrayAdapter(MiAlerta.this, android.R.layout.simple_list_item_1, listaalertas);
                    listaAlertas.setAdapter(adapter);
                } else {
                    Toast.makeText(MiAlerta.this, peticion.detalle, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Alertas_usuario> call, Throwable t) {
                Toast.makeText(MiAlerta.this,"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void CrearAlerta() {

        servicioPeticion service = Api.getApi(MiAlerta.this).create(servicioPeticion.class);
        Call<Peticion_crear_alerta> AlertasCall= service .Crear_alerta(idusuario);
        AlertasCall.enqueue(new Callback<Peticion_crear_alerta>() {

            @Override
            public void onResponse(Call<Peticion_crear_alerta> call, Response<Peticion_crear_alerta> response) {
                Peticion_crear_alerta peticion = response.body();
                if (response.body() == null) {
                    Toast.makeText(MiAlerta.this, "Ocurrio un error, intentalo más tarde", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (peticion.estado == "true") {
                    Toast.makeText(MiAlerta.this, "Alerta creada correctamente", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(MiAlerta.this, peticion.detalle, Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<Peticion_crear_alerta> call, Throwable t) {
                Toast.makeText(MiAlerta.this,"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private void obtenerinformacion(Integer id, String creacion, String actualizacion) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MiAlerta.this);
        builder.setTitle("Id de usuario: "+id );
        builder.setMessage("Fecha de creacion : "+creacion +"\n" +
                "Fecha de actualizacion: "+actualizacion);
        builder.setPositiveButton("Aceptar", null);

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}

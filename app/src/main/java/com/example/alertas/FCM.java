package com.example.alertas;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.example.alertas.Activities.MainActivity;
import com.example.alertas.Api.Api;
import com.example.alertas.Servicios.servicioPeticion;
import com.example.alertas.ViewModels.Peticion_crear_alerta;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.NotNull;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;



import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FCM extends FirebaseMessagingService {

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
        Log.e("token", "mi token es: " + s);
        guardarTokenNuevo(s);
    }
    public void guardarTokenNuevo(String s){
        DatabaseReference ref= FirebaseDatabase.getInstance().getReference().child("token");
        ref.child("Camperrote").setValue(s);
    }
    String idAlerta;
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        String from = remoteMessage.getFrom();
        Log.e("TAG","Mensaje Recibido de: "+from);
        /*if(remoteMessage.getNotification() != null){
            Log.e("TAG","Titulo: "+remoteMessage.getNotification().getTitle());
            Log.e("TAG","Body: "+remoteMessage.getNotification().getBody());
        } // -- Clave valor*/
        if(remoteMessage.getData().size() > 0){
            Log.e("TAG","Mi titulo es "+remoteMessage.getData().get("Alerta"));
            Log.e("TAG","Mi detalle es "+remoteMessage.getData().get("idAlerta"));
            String Alerta = remoteMessage.getData().get("Alerta");
            idAlerta= remoteMessage.getData().get("idAlerta");
            mayorQueOreo(Alerta,idAlerta);
        }
    }
    private String Usuariotxt;
    private int idusuario=0;
    private int IDALERTA;

    private void mayorQueOreo(String Alerta, String idAlerta) {
        String id="Mensaje";
        IDALERTA=Integer.parseInt(idAlerta);
        NotificationManager nm =(NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder=new NotificationCompat.Builder(this,id);
        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel nc = new NotificationChannel(id,"Nuevo", NotificationManager.IMPORTANCE_HIGH);
            nc.setShowBadge(true);
            assert nm !=null;
            nm.createNotificationChannel(nc);
        }
        builder.setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentTitle(Alerta)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentText(idAlerta)
                .setContentIntent(clikcnoti())
                .setContentInfo("Nuevo");
        Random random = new Random();
        int idNotify = random.nextInt(8000);

        assert nm!=null;
        nm.notify(idNotify,builder.build());

    }

    private PendingIntent clikcnoti() {
        SharedPreferences preferencias= getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token=preferencias.getString("Token","");
        Intent it;
        if (token !=""){
            it=new Intent(getApplicationContext(), Menu.class);
        }else{
            it=new Intent(getApplicationContext(), MainActivity.class);
        }
        it.putExtra("idAlerta",idAlerta);
        CrearAlerta();
        it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return PendingIntent.getActivity(this,0, it,0);
    }
    public void CrearAlerta() {
        obtenerid();
        servicioPeticion service = Api.getApi(this).create(servicioPeticion.class);
        Call<Peticion_crear_alerta> visualizacionCall= service .Crear_visualizacion(idusuario,IDALERTA);
        visualizacionCall.enqueue(new Callback<Peticion_crear_alerta>() {
            @Override
            public void onResponse(Call<Peticion_crear_alerta> call, Response<Peticion_crear_alerta> response) {
                Peticion_crear_alerta peticion=response.body();
                if(response.body()==null){
                    Toast.makeText(getApplication(),"Ocurrio un error, intentalo más tarde",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(peticion.estado == "true"){
                    Toast.makeText(getApplication(),"Visualizacion creada correctamente",Toast.LENGTH_SHORT).show();

                }else{
                    Toast.makeText(getApplication(),peticion.detalle,Toast.LENGTH_SHORT).show();
                }

            }
            @Override
            public void onFailure(Call<Peticion_crear_alerta> call, Throwable t) {
                Toast.makeText(getApplication(),"No podemos conectarnos :c", Toast.LENGTH_SHORT).show();
            }
        });
    }
    public void obtenerid(){
        SharedPreferences preferencias=getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        Usuariotxt=preferencias.getString("Idusuario","");
        idusuario= Integer.parseInt(Usuariotxt);
    }

}